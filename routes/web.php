<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'dashboardController@index']);
Route::get('employees', ['as' => 'employees', 'uses' => 'EmployeeController@viewemployees']);
Route::get('holidays', ['as' => 'holidays', 'uses' => 'HolidayController@index']);
Route::get('leaveyear', ['as' => 'leaveyear', 'uses' => 'LeaveyearController@index']);
Route::get('newemployee', ['as' => 'newemployee', 'uses' => 'EmployeeController@index']);

Route::post('saveemployee', ['as' => 'saveemployee', 'uses' => 'EmployeeController@save']);
Route::post('updateemployee', ['as' => 'updateemployee', 'uses' => 'Employee@update']);
Route::post('deleteemployee', ['as' => 'deleteemployee', 'uses' => 'EmployeeController@delete']);

Route::post('saveleaveyear', ['as' => 'saveleaveyear', 'uses' => 'LeaveyearController@save']);
Route::post('updateleaveyear', ['as' => 'updateleaveyear', 'uses' => 'LeaveyearController@update']);
Route::post('deleteleaveyear', ['as' => 'deleteleaveyear', 'uses' => 'LeaveyearController@delete']);

Route::get('getholidayentitlement/{leaveType}/{contractStartdate}/{contractEnddate}', ['as' => 'getholidayentitlement', 'uses' => 'HolidayController@getholidayEntitlement']);

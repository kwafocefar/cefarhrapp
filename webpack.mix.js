const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/controllers/dashboardController.js', 'public/js/controllers')
    .js('resources/js/controllers/employeesController.js', 'public/js/controllers')
    .js('resources/js/controllers/holidaysController.js', 'public/js/controllers')
    .js('resources/js/controllers/leaveyearController.js', 'public/js/controllers')
    .js('resources/js/controllers/newemployeeController.js', 'public/js/controllers')
    .sass('resources/sass/app.scss', 'public/css');

/**
 * Created by nanakwafo on 11/03/2020.
 */
$(function(){
    
    $( "#getholidayentitlement" ).click(function(e) {
        e.preventDefault();
        var leaveType =$('#leaveType').val();
        var contractStartdate =$('#contractstartyear').val();
        var contractEnddate =$('#contractendyear').val();

        $.ajax({
          type: 'GET',
          url: "/hrapp/public/getholidayentitlement/" + leaveType +"/"+contractStartdate +"/" + contractEnddate,
          
          success: function (data) {
             
              $('#Employeeholidayentitlement').val(data);


          }
      })

      });
})
/**
 * Created by nanakwafo on 11/03/2020.
 */
$(function(){

    $('#leaveyear').DataTable({
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        }
    });
})
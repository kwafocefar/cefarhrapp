
<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="index.html" class="aside-logo">Cefar<span>HR</span></a>
        <a href="" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="" class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></a>

            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0">Kwafo Nana Mensah</h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0">Administrator</p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="edit"></i> <span>Edit Profile</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="user"></i> <span>View Profile</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="settings"></i> <span>Account Settings</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="help-circle"></i> <span>Help Center</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
            <li class="nav-label">Dashboard</li>
            <li class="nav-item active"><a href="dashboard" class="nav-link"><i data-feather="shopping-bag"></i> <span>Human Resource</span></a></li>



            <li class="nav-label mg-t-25">Management</li>
            <li class="nav-item with-sub">
                <a href="" class="nav-link"><i data-feather="user"></i> <span>Employees</span></a>
                <ul>
                    <li><a href="newemployee">New Employee</a></li>
                    <li><a href="employees">View Employee</a></li>
                    <li><a href="holidays">Employee Holidays</a></li>

                </ul>
            </li>
            <li class="nav-item with-sub">
                <a href="" class="nav-link"><i data-feather="file"></i> <span>Settings</span></a>
                <ul>
                    <li><a href="leaveyear">Annual leave year</a></li>
                </ul>
            </li>
            {{--<li class="nav-label mg-t-25">Reports</li>--}}
            {{--<li class="nav-item"><a href="components" class="nav-link"><i data-feather="layers"></i> <span>Components</span></a></li>--}}
            {{--<li class="nav-item"><a href="collections" class="nav-link"><i data-feather="box"></i> <span>Collections</span></a></li>--}}
        </ul>
    </div>
</aside>
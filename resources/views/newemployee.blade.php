@extends('layout.app')
@section('title','New Employee')
@section('content')

    <div class="container ">
        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">New Employee</li>
                    </ol>
                </nav>
                <h4 class="mg-b-0 tx-spacing--1">Add a new Employee</h4>
            </div>

        </div>

        <div class="row ">
            <form action="saveemployee" method="post">
                {{csrf_field()}}
            <div class="col-sm">

                <div class="form-group">
                    <label class="d-block">Firstname</label>
                    <input type="text" name="firstname" class="form-control" placeholder="Enter your firstname">
                </div>
                <div class="form-group">
                    <label class="d-block">Lastname</label>
                    <input type="text" name="lastname" class="form-control" placeholder="Enter your lastname">
                </div>

                <div class="form-group">
                    <label class="d-block">Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder="Enter your Phone Number">
                </div>

            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label class="d-block">Leave Type</label>
                    <select class="custom-select" name="leavetype" id="leaveType">
                        <option value="">Choose Leave Type</option>
                       @foreach(\App\Leaveyear::all() as $leavetype)
                            <option value="{{$leavetype->id}}" >{{$leavetype->name}}</option>
                           @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block">Contract Date</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Date</span>
                        </div>
                        <input type="date" name="startdate" id="contractstartyear" aria-label="startdate" class="form-control"
                               placeholder="start">
                        <input type="date" name="enddate"  id="contractendyear"aria-label="enddate" class="form-control" placeholder="end">
                    </div>
                </div>
                <button  id="getholidayentitlement">Get Holiday Entitlement</button>
                <div class="form-group">
                    <label class="d-block">Holiday Entitlement</label>
                    <input type="text" name="holidayentitlement" class="form-control" id="Employeeholidayentitlement" placeholder="Enter your holidayentitlement">
                </div>
            </div>

            <div class="row" style="float: right">
                <button type="submit" class="btn btn-outline-primary">Save</button>
            </div>
            </form>
        </div><!-- row -->

    </div><!-- container -->
@endsection
<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="description" content="Cefarhr">
    <meta name="author" content="Cefardev">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <title>@yield('title')</title>

    <link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/jqvmap/jqvmap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/dashforge.css">
    @if($routeName=='dashboard')
    <link rel="stylesheet" href="assets/css/dashforge.dashboard.css">
        @elseif($routeName=='employees')
        <link href="lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">

    @endif
</head>
<body>

@include('partials.profiledetail')

<div class="content ht-100v pd-0">
    <div class="content-header">
        <div class="content-search">

        </div>
        <nav class="nav">
            <a href="" class="nav-link"><i data-feather="help-circle"></i></a>
            <a href="" class="nav-link"><i data-feather="grid"></i></a>
            <a href="" class="nav-link"><i data-feather="align-left"></i></a>
        </nav>
    </div><!-- content-header -->

    <div class="content-body">
        @yield('content')
    </div>
</div>

<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/feather-icons/feather.min.js"></script>
<script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

@if($routeName=='employees'||'leaveyear')
<script src="lib/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
<script src="lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
@endif


<script src="lib/jquery.flot/jquery.flot.js"></script>
<script src="lib/jquery.flot/jquery.flot.stack.js"></script>
<script src="lib/jquery.flot/jquery.flot.resize.js"></script>
<script src="lib/chart.js/Chart.bundle.min.js"></script>
<script src="lib/jqvmap/jquery.vmap.min.js"></script>
<script src="lib/jqvmap/maps/jquery.vmap.usa.js"></script>

<script src="assets/js/dashforge.js"></script>
<script src="assets/js/dashforge.aside.js"></script>
<script src="assets/js/dashforge.sampledata.js"></script>
<script src="assets/js/dashboard-one.js"></script>

<!-- append theme customizer -->
<script src="lib/js-cookie/js.cookie.js"></script>
<script src="assets/js/dashforge.settings.js"></script>
<script src="{{asset('js/controllers/'.$routeName.'Controller.js')}}"></script>

</body>
</html>

@extends('layout.app')
@section('title','Employees')
@section('content')

    <div class="container pd-x-0">
        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">All Employees</li>
                    </ol>
                </nav>
                <h4 class="mg-b-0 tx-spacing--1">All Employees</h4>
            </div>

        </div>

        <div class="row ">

            <div class="col-12">
                <div data-label="Example" class="df-example demo-table">
                    <table id="employees" class="table">
                        <thead>
                        <tr>
                            <th class="wd-20p">Firstname</th>
                            <th class="wd-25p">Lastname</th>
                            <th class="wd-20p">Phone</th>
                            <th class="wd-15p">Leavetype</th>
                            <th class="wd-20p"> Start Date</th>
                            <th class="wd-20p"> End Date</th>
                            <th class="wd-20p"> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                        <tr>
                            <td>{{$employee->firstname}}</td>
                            <td>{{$employee->lastname}}</td>
                            <td>{{$employee->phone}}</td>
                            <td>{{\App\Leaveyear::find($employee->leavetype)->name}}</td>
                            <td>{{$employee->startdate}}</td>
                            <td>{{$employee->enddate}}</td>
                            <td>
                            <button type="button" class="btn btn-outline-danger">Edit</button>
                                <button type="button" class="btn btn-outline-warning">Delete</button>
                            
                            </td>
                        </tr>
                        @endforeach
                      

                     </tbody>
                    </table>
                </div><!-- df-example -->

            </div>



        </div><!-- row -->
    </div><!-- container -->
@endsection
@extends('layout.app')
@section('title','Leave year')
@section('content')

    <div class="container pd-x-0">
        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Leave year</li>
                    </ol>
                </nav>
                <h4 class="mg-b-0 tx-spacing--1">Leave Year</h4>
            </div>

        </div>

        <div class="row ">
            <form method="post" action="saveleaveyear">
                {{csrf_field()}}
            <div class="col-sm">

                <div class="form-group">
                    <label class="d-block">Leave Year Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <label class="d-block">Leave Year Date</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Date</span>
                        </div>
                        <input type="date" name="startdate" aria-label="startdate" class="form-control"
                               placeholder="start">
                        <input type="date" name="enddate" aria-label="enddate" class="form-control" placeholder="end">
                    </div>
                </div>

                <div class="form-group">
                    <label class="d-block">holidayEntitlement</label>
                    <input type="text" name="holidayEntitlement" class="form-control" placeholder="Enter your Phone Number">
                </div>
                <button type="submit" class="btn btn-outline-primary">Save</button>
            </div>
            </form>


        </div><!-- row -->




        <div class="row ">

            <div class="col-12">
                <div data-label="Example" class="df-example demo-table">
                    <table id="leaveyear" class="table">
                        <thead>
                        <tr>
                            <th class="wd-20p">Name</th>
                            <th class="wd-25p">Start Date</th>
                            <th class="wd-20p">End Date</th>
                            <th class="wd-15p">Entitlement</th>
                            <th class="wd-15p">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($leaveyears as $leaveyear)
                        <tr>
                            <td>{{$leaveyear->name}}</td>
                            <td>{{$leaveyear->startdate}}</td>
                            <td>{{$leaveyear->enddate}}</td>
                            <td>{{$leaveyear->holidayEntitlement}}</td>
                            <td><button type="button" class="btn btn-outline-danger">Edit</button>
                                <button type="button" class="btn btn-outline-warning">Delete</button>
                            </td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- df-example -->

            </div>



        </div><!-- row -->
    </div><!-- container -->
@endsection
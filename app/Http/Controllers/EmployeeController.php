<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('newemployee', [
            'routeName' => parent::getRouteName(),
        ]);
    }

    public function viewemployees()
    {
        return view('employees', [
            'routeName' => parent::getRouteName(),
            'employees' => Employee::all(),
        ]);
    }

    public function save(Request $request)
    {
        $employee = new Employee();
        $employee->firstname = $request['firstname'];
        $employee->lastname = $request['lastname'];
        $employee->phone = $request['phone'];
        $employee->leavetype = $request['leavetype'];
        $employee->startdate = $request['startdate'];
        $employee->enddate = $request['enddate'];
        $employee->holidayentitlement = $request['holidayentitlement'];
        $employee->save();

        return  \redirect('newemployee');
    }

    public function update(Request $request)
    {
        $employee = Employee::find($request['id']);
        $employee->firstname = $request['firstnameEdit'];
        $employee->lastname = $request['lastnameEdit'];
        $employee->phone = $request['phoneEdit'];
        $employee->leavetype = $request['leavetypeEdit'];
        $employee->startdate = $request['startdateEdit'];
        $employee->enddate = $request['enddateEdit'];
        $employee->save();
    }

    public function delete(Request $request)
    {
        $employee = Employee::find($request['id']);
        $employee->delete();
    }
}

<?php

namespace App\Http\Controllers;

use App\Leaveyear;
use Illuminate\Http\Request;

class LeaveyearController extends Controller
{
    public function index()
    {
        return view('leaveyear', [
            'routeName' => parent::getRouteName(),
            'leaveyears' => Leaveyear::all(),
        ]);
    }

    public function save(Request $request)
    {
        $leaveyear = new Leaveyear();
        $leaveyear->name = $request['name'];
        $leaveyear->startdate = $request['startdate'];
        $leaveyear->enddate = $request['enddate'];
        $leaveyear->holidayEntitlement = $request['holidayEntitlement'];
        $leaveyear->save();

        return \redirect('leaveyear');
    }

    public function update(Request $request)
    {
        $leaveyear = Leaveyear::find($request['idEdit']);
        $leaveyear->name = $request['nameEdit'];
        $leaveyear->startdate = $request['startdateEdit'];
        $leaveyear->enddate = $request['enddateEdit'];
        $leaveyear->holidayEntitlement = $request['holidayEntitlementEdit'];
        $leaveyear->save();
    }

    public function delete(Request $request)
    {
        $employee = Leaveyear::find($request['idDelete']);
        $employee->delete();
    }
}

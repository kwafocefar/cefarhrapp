<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Leaveyear;

class HolidayController extends Controller
{
    public function index()
    {
        return view('holidays', [
            'routeName' => parent::getRouteName(),
        ]);
    }

    public function getholidayEntitlement($leaveType, $contractStartdate, $contractEnddate)
    {
        $leaveyear = Leaveyear::find($leaveType)->first();
        $holidayEntitlement = $leaveyear->holidayEntitlement;
        $leaveyearstartdate = $leaveyear->startdate;
        $leaveyearenddate = $leaveyear->enddate;

        $to = Carbon::createFromFormat('Y-m-d', $leaveyearenddate);
        $from = Carbon::createFromFormat('Y-m-d', $leaveyearstartdate);
        $diff_in_days = $to->diffInDays($from);

        $to_e = Carbon::createFromFormat('Y-m-d', $contractEnddate);
        $from_e = Carbon::createFromFormat('Y-m-d', $contractStartdate);
        $diff_in_days_e = $to_e->diffInDays($from_e);

        $result = ($diff_in_days_e / $diff_in_days) * 100;

        $result2 = ($result / 100) * $holidayEntitlement;

        return round($result2);
    }
}
